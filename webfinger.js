
var bots = {
	"echo": {
		"apfeed": "https://openwhisk.ng.bluemix.net/api/v1/web/ted%40gould.cx_dev/echo/"
	}
}

var resourceRegExp = new RegExp(/resource=([^&]*)/);
var botnameRegExp = new RegExp(/acct:(.*)@bots.gould.cx/);

function main(params) {
	var resourceResult = params["__ow_query"].match(resourceRegExp);
	if (!resourceResult || resourceResult.length < 2) {
		return { "statusCode": 400 };
	}

	var nameResult = resourceResult[1].match(botnameRegExp);
	if (!nameResult || nameResult.length < 2) {
		return { "statusCode": 400 };
	}

	var entry = bots[nameResult[1]];
	if (!entry) {
		return { "statusCode": 404 };
	}

	return {
		"body": {
				"subject": "acct:" + nameResult[1] + "@bots.gould.cx",
				"links": [
					{
					"rel": "self",
					"type": "application/activity+json",
					"href": entry.apfeed
					}
				]
		},
		"headers": {
			"Content-Type": "application/jrd+json"
		}
	};
}
