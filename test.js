fs = require('fs'); 
assert = require('assert');

testscript = process.argv[2];
inputname = process.argv[3];
outputname = process.argv[4];

eval(fs.readFileSync(testscript).toString());

input = JSON.parse(fs.readFileSync(inputname).toString());
output = JSON.parse(fs.readFileSync(outputname).toString());

mainout = main(input);

assert.deepEqual(mainout, output);
